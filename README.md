# C++ macros: Tool for using C++ functions in PyROOT analysis

 By Ivan Cambon Bouzas

 ## Introduction
 
 In this repository we include several C++ macros designed for HEP analysis. The goal of them is to predefine some compatiled C++ functions which can be used in PyROOT codes through __ROOT.gROOT.ProcessLine__. For example, we can have predefine functions that can be used in TTree or RDataFrames objects to create new branches. 

## Requirements

To compile the macros, only a ROOT installation is required. The ROOT version does not matter at all, but is recommended to use newest one

## Actual macros

1. **HEP_Exp_Tool**

This folder includes several macros which are useful for high enery physics analysis. They can be used in RDataFrame.Define method. Currently we have

- *Kinematics.C*: It has functions to compute kinematical quantities such as $\Delta R$ 

2. **RooFit_PDFs**

This folder includes a list of RooFit PDFs that were defined with the usual RooClassFactory method.