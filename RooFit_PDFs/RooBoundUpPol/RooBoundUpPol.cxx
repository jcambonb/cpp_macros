#include "Riostream.h" 

#include "RooBoundUpPol.h" 
#include "RooAbsReal.h" 
#include "RooAbsCategory.h" 
#include <math.h> 
#include "TMath.h" 


ClassImp(RooBoundUpPol)

RooBoundUpPol::RooBoundUpPol(const char *name, const char *title,
                             RooAbsReal& _x, 
                             RooAbsReal& _m0,
                             RooAbsReal& _mf, 
                             const RooArgList& _coeffs) :
    RooAbsPdf(name, title),
    x("x", "x", this, _x),
    m0("m0", "m0", this, _m0),
    mf("mf", "mf", this, _mf),
    coeffs("coeffs", "coeffs", this)
{
  // copy over coefficeints
  TIterator* coefIter = _coeffs.createIterator() ;
  RooAbsArg* coef(0) ;
  while ((coef = (RooAbsArg*)coefIter->Next())) {
    if (!dynamic_cast<RooAbsReal*>(coef)) {
      cout << "RooBoundUpPol::ctor(" << GetName() << ") ERROR: coefficient " << coef->GetName() 
           << " is not of type RooAbsReal" << endl ;
      assert(0) ;
    }
    coeffs.add(*coef) ;
  }
  delete coefIter ;
}

RooBoundUpPol::RooBoundUpPol(const RooBoundUpPol& other, 
                             const char* name):
    RooAbsPdf(other, name),
    x("x", this, other.x),
    m0("m0", this, other.m0),
    mf("mf", this, other.mf),
    coeffs("coeffs", this, other.coeffs)
{

}

Double_t RooBoundUpPol::evaluate() const
{
    if (x <= m0) {return(0);}
    if (x > mf) {return(0);}
    Double_t result(0);

    TIterator* iter = coeffs.createIterator();
    RooRealVar* coef(0);
    Int_t n(0);
    while ((coef = (RooRealVar*) iter->Next())) {
        n++;
        result += coef->getVal() * TMath::Power((x-m0), n);
    }
    delete iter;

    return(result);

}