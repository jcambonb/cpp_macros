/*****************************************************************************
 * Project: BaBar detector at the SLAC PEP-II B-factory
 * Package: RooRarFit
 *    File: $Id: RooThreshold.rdl,v 1.1 2012/04/02 14:47:56 fwilson Exp $
 * Authors: Fergus Wilson
 * History:
 *
 * Copyright (C) 2005-2012, RAL
 *****************************************************************************/

#ifndef ROOTHRESHOLD
#define ROOTHRESHOLD

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"

class RooRealVar;
class RooArgList;

class RooThreshold : public RooAbsPdf {

public:
  RooThreshold(const char *name, const char *title,
               RooAbsReal& _x, RooAbsReal& _m0, 
               RooAbsReal& _power, const RooArgList& _coeffs);

  RooThreshold(const RooThreshold& other, 
                    const char* name=0) ;
  virtual TObject* clone(const char* newname) const { 
    return new RooThreshold(*this, newname); 
  }
  inline virtual ~RooThreshold() { }

protected:

  RooRealProxy x ;     // 
  RooRealProxy m0 ;  // threshold value
  RooRealProxy power ;  // power
  RooListProxy coeffs ; // 
 
  Double_t evaluate() const ;

private:
 
  ClassDef(RooThreshold, 0) // Threshold PDF 
};

#endif